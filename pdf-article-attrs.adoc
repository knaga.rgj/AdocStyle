//
// [注意] 空行を入れるとバグるかも
//   2021/02/21 knaga.rgj@gmail.com
//
:scripts: cjk
:pdf-fontsdir: adoc-style
:pdf-stylesdir: adoc-style
:pdf-theme: pdf-article-theme.yml
:doctype: article
:stem: asciimath
:source-highlighter: rouge
:sectnums:
:mathematical-format: svg

<!-- [GitLab版 GFM](https://docs.gitlab.com/ee/user/markdown.html)   -->
<!-- [GitHub版 GFM](https://github.github.com/gfm/) -->

# adoc-style

asciidoctor-pdf で pdf 作成する用の標準設定

ドキュメントディレクトリ下に本リポジトリのファイル群を adoc-style というディレクトリ名で置くことを前提としている。

参考: https://github.com/asciidoctor/asciidoctor-pdf/blob/master/docs/theming-guide.adoc  
謝辞: https://tech-blog.cloud-config.jp/2020-05-11-github-actions-asciidoc-pdf2/


## テンプレート

```
Taro <taro@example.com>
v 1.0, 2021/01/01
// [注意] ヘッダ部で空行を入れるとバグる
include::adoc-style/pdf-article-attrs.adoc[]
//:toc:

= 文書タイトルです

== はじめに

こんにちは

```

## asciidoctor-pdf 標準構成 [TODO]

diagram は 2021/02/21 時点では plantuml, graphviz, gnuplot のいずれかで。

数式は asciimath を指定しているけど、latex 形式も動くっぽい謎

ページヘッダ、ページフッタ、タイトル書式はこれから[TODO]。footnote は文書末尾になるかも[TODO]。

Ubutu/debian で必要なパッケージ群を入れておく [TODO]
 - `sudo apt install libpango1.0-dev make bison flex cmake fontx-lyx ruby-dev`
 - `sudo apt install graphviz plantuml gnuplot`
 - ひょっとしたら build-essential もいるかも？

gem で asciidoctor-pdf asciidoctor-mathematical asciidoctor-diagram rouge を入れる。

バージョン関係の整理はこれから[TODO]。2021/02/21 時点では下記。

```
asciidoctor (2.0.12)
asciidoctor-diagram (2.1.0)
asciidoctor-diagram-ditaamini (0.13.1)
asciidoctor-diagram-plantuml (1.2021.0)
asciidoctor-mathematical (0.3.5)
asciidoctor-pdf (1.5.4)
rouge (3.26.0)
```





## フォント

源様明朝
 - 配布元: https://github.com/ButTaiwan/genyo-font/
 - ライセンス: SIL Open Font License 1.1 
 - GenYoMinJP/README.md 参照

源真ゴシック
 - 配布元: http://jikasei.me/font/genshin/
 - ライセンス: SIL Open Font License 1.1
 - GenShinGothic/README_GenShin.txt 参照

Noto emoji
 - 配布元: https://www.google.com/get/noto/#emoji-zsye
 - ライセンス: SIL Open Font License 1.1
 - NotoEmoji/README 参照

なんちゃって斜体
 - 配布元(手前味噌): https://gitlab.com/knaga.rgj/GenX-Pseudo-Italic
 - ライセンス: SIL Open Font License 1.1
 - [GenX-NanSha/README.md](https://gitlab.com/knaga.rgj/GenX-Pseudo-Italic/) 参照

#!/bin/sh -x
# tested 2021/02/23 on debian/bullseye/amd64 (testing)

# README.md があちこちにあるので混同しないように注意

#sudo apt install wget unzip p7zip

GY_CID=bc2fa246196fefc1ef9e9843bc8cdba22523a39d
GY_BASE=genyo-font-${GY_CID}
GY_FILES_ARCHIVED="${GY_BASE}/README.md ${GY_BASE}/SIL_Open_Font_License_1.1.txt ${GY_BASE}/JP/GenYoMinJP-Regular.ttf ${GY_BASE}/JP/GenYoMinJP-Bold.ttf"

GS_ARCHIVE=genshingothic-20150607.7z
GS_FILES="GenShinGothic-P-Regular.ttf GenShinGothic-P-Bold.ttf README_GenShin.txt SIL_Open_Font_License_1.1.txt"

NT_ARCHIVE=NotoEmoji-unhinted.zip
NT_FILES="NotoEmoji-Regular.ttf README LICENSE_OFL.txt"


wget -O ${GY_BASE}.zip https://github.com/ButTaiwan/genyo-font/archive/${GY_CID}.zip
unzip ${GY_BASE}.zip ${GY_FILES_ARCHIVED} 

mkdir GenYoMinJP/
mv ${GY_FILES_ARCHIVED} GenYoMinJP/

wget -O ${GS_ARCHIVE} "https://onedrive.live.com/download?resid=9882014220136771%21342"
7zr x ${GS_ARCHIVE} ${GS_FILES}

mkdir GenShinGothic/
mv ${GS_FILES} GenShinGothic/


wget https://noto-website-2.storage.googleapis.com/pkgs/${NT_ARCHIVE}
unzip -j ${NT_ARCHIVE} ${NT_FILES}

mkdir NotoEmoji/
mv ${NT_FILES} NotoEmoji/
